const colors = {
  brand: "#c30006",
  brandHover: "#970c11",
  white: "#fff",
  grey: "#e6e6e6",
  black: "#000",
};

export default {
  colors,
};
