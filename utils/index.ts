const base = 10;

export const dim = (dim: number = 1) => `${base * dim}px`;
