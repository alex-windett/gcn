import Axios from "axios";
import React, { useState, useEffect } from "react";
import Header from "../components/Header";
import Button from "../components/Button";
import Footer from "../components/Footer";
import { dim } from "../utils";

import { VideoInterface } from "../types";

import styled from "styled-components";

const StyledButton = styled(Button)`
  position: fixed;
  bottom: ${dim()};
  right: ${dim()};
`;

const Layout: React.FC<{}> = ({ children }) => {
  const [activeNav, setactiveNav] = useState(false);
  const [activeSearch, setactiveSearch] = useState(false);
  const [searchTerm, setsearchTerm] = useState("");
  const [searchResults, setsearchResults] = useState([]);

  const handleNavToggle = () => setactiveNav(!activeNav);
  const handleSearchToggle = () => setactiveSearch(!activeSearch);
  const handleSearchTerm = async (event: React.FormEvent<HTMLInputElement>) => {
    // @ts-ignore
    const { value } = event.target;
    await setsearchTerm(value);

    if (value.length < 3) return;

    try {
      const { data } = await Axios({
        method: "get",
        url: "/api/videos",
      });

      const results: VideoInterface[] = data.filter((result: VideoInterface) => {
        return result.title.toLowerCase().includes(value.toLowerCase());
      });

      // @ts-ignore
      setsearchResults(results);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", () => {
      setactiveNav(false);
    });
  }, []);

  const handleTop = () => {
    window.scrollTo({ behavior: "smooth", top: 0 });
  };

  return (
    <>
      <Header
        activeNav={activeNav}
        handleNavToggle={handleNavToggle}
        activeSearch={activeSearch}
        handleSearchToggle={handleSearchToggle}
        setSearchTerm={handleSearchTerm}
        searchTerm={searchTerm}
        searchResults={searchResults}
      />
      <div>{children}</div>

      <Footer />

      <StyledButton onClick={handleTop}>To Top</StyledButton>
    </>
  );
};

export default Layout;
