export interface VideoInterface {
  playlist: any[];
  topics: any[];
  public?: boolean;
  _id: string;
  _v: number;
  description: string;
  publishedDate: string;
  title: string;
  urlTitle: string;
}
