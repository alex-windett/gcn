import styled, { css } from "styled-components";
import Link from "next/link";

import styles from "../../utils/styles";
import { dim } from "../../utils";
import { VideoInterface } from "../../types";

const Grid = styled.article`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: ${dim(2)};
`;

const Title = styled.h5`
  margin: 0;
`;

const Item = styled.div`
  display: flex;

  grid-column: span 1;
  padding: ${dim(2)} 0;
  flex-direction: column;

  ${({ inline }: { inline: boolean | undefined }) =>
    inline
      ? css`
          grid-column: span 2;
          display: grid;
          grid-template-columns: repeat(3, 1fr);
          grid-column-gap: ${dim()};

          ${Title} {
            grid-column: span 2;
          }
        `
      : css`
          &:nth-child(3n + 1) {
            grid-column: span 2;
            border-top: 1px solid ${styles.colors.grey};

            border-bottom: 1px solid ${styles.colors.grey};
          }
        `}
`;

const Img = styled.img`
  width: 100%;
  margin-bottom: ${dim()};
`;

interface Props {
  videos: VideoInterface[];
  inline?: boolean;
}

export default ({ videos, inline }: Props) => {
  return (
    <Grid>
      {videos.map((video: VideoInterface, i: number) => (
        <Item key={i} inline={inline}>
          <Link href="/video/[slug]" as={`/video/${video.urlTitle}`}>
            <a>
              <Img
                src={`https://img.youtube.com/vi/${video._id}/mqdefault.jpg`}
                alt={video.title}
                title={video.title}
              />
            </a>
          </Link>
          <Title>{video.title}</Title>
        </Item>
      ))}
    </Grid>
  );
};
