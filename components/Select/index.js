import styled from "styled-components";
import { dim } from "../../utils";
import styles from "../../utils/styles";

const Select = styled.select`
  border: 1px solid ${styles.colors.grey};
  width: 100%;
  margin: ${dim()} 0 ${dim(2)};
  padding: ${dim()};
`;

export default ({ children, ...props }) => <Select {...props}> {children}</Select>;
