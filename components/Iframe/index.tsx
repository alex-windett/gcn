import styled from "styled-components";

interface Props {
  src: string;
}

const Div = styled.div`
  position: relative;
  padding-bottom: 56.25%;
  height: 0;
  overflow: hidden;
`;

const Iframe = styled.iframe`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

export default ({ src }: Props) => (
  <Div>
    <Iframe src={src}></Iframe>
  </Div>
);
