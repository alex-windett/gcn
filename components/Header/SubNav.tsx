import styled, { css } from "styled-components";
import Link from "next/link";
import { useState } from "react";

import { dim } from "../../utils";
import styles from "../../utils/styles";

const NavList = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

const NavListItem = styled.li`
  margin: 0;
  padding: ${dim()} 0;
  & + & {
    border-top: 1px solid ${styles.colors.grey};
  }
`;

const NavListItemSecondary = styled(NavListItem)`
  padding: 0;
  transition: ease-in all 0.5s;

  ${({ isOpen }: { isOpen: boolean }) =>
    isOpen
      ? css`
          overflow: unset;
          max-height: 500px;
        `
      : css`
          max-height: 0;
          overflow: hidden;
        `};
`;

const NavSecondaryList = styled(NavList)`
  padding: 0;
  margin-left: ${dim(2)};
`;

export default ({ item }: { item: { title: string; items: any[] } }) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggleSecondaryItems = () => setIsOpen(!isOpen);
  return (
    <>
      <NavListItem onClick={toggleSecondaryItems}>{item.title}</NavListItem>

      <NavListItemSecondary isOpen={isOpen}>
        <NavSecondaryList>
          {item.items.map((secondary, i) => (
            <NavListItem key={i}>
              <Link href="/post/[id]" as={`/post/${secondary.title}`}>
                <a>{secondary.title}</a>
              </Link>
            </NavListItem>
          ))}
        </NavSecondaryList>
      </NavListItemSecondary>
    </>
  );
};
