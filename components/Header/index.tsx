import * as React from "react";
import styles from "../../utils/styles";
import Nav from "./Nav";
import styled from "styled-components";
import { dim } from "../../utils";
import Link from "next/link";
import Button from "../Button";
import Videos from "../VideosGrid";

interface Props {
  activeNav: boolean;
  handleNavToggle: () => void;
  activeSearch: boolean;
  handleSearchToggle: () => void;
  searchTerm: string;
  setSearchTerm: (e: any) => void;
  searchResults: any[];
}

const Wrapper = styled.div`
  background-color: ${styles.colors.brand};
  padding: ${dim(1)} 0;
`;

const Logo = styled.div`
  padding: ${dim()};
  grid-column: 3 / span 3;
`;

const Header = styled.header`
  display: grid;
  grid-template-columns: repeat(7, minmax(0, 1fr));
  justify-content: space-between;
  align-items: center;
  position: relative;
  width: 100%;
  z-index: 1;
`;

const Search = styled.div`
  grid-column: 7;
  justify-content: flex-end;
  display: flex;
  margin-right: 10px;

  img {
    width: 35px;
  }
`;

const QuickNav = styled.nav`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  flex-direction: row;
  justify-content: space-around;
  border-top: 1px solid ${styles.colors.white};
  margin: 0 ${dim(2)};
  text-align: center;
  padding-top: ${dim()};
`;

const P = styled.p`
  margin: 0;
  padding: ${dim()} 0;

  * {
    color: ${styles.colors.white};
    text-decoration: none;
  }

  & + & {
    border-left: 1px solid ${styles.colors.white};
  }
`;

const SearchButton = styled.button`
  appearance: none;
  background: transparent;
  border: none;
`;

const SearchView = styled.article`
  position: fixed;
  background: ${styles.colors.white};
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  z-index: 1;
  padding: ${dim(2)};
  overflow: scroll;
`;

const SearchInput = styled.input`
  width: 100%;
  appearance: none;
  border: solid 1px ${styles.colors.black};
  padding: ${dim()};
`;

const CloseButton = styled(Button)`
  position: absolute;
  top: ${dim()};
  right: ${dim(1)};
`;

export default ({
  activeNav,
  handleNavToggle,
  searchTerm,
  setSearchTerm,
  activeSearch,
  handleSearchToggle,
  searchResults,
}: Props) => {
  const activeClass = activeNav ? "is-active" : "";

  return (
    <>
      <Wrapper>
        <Header>
          <div>
            <button
              className={`hamburger hamburger--spin ${activeClass}`}
              type="button"
              onClick={handleNavToggle}
            >
              <span className="hamburger-box">
                <span className="hamburger-inner"></span>
              </span>
            </button>
          </div>

          <Link href="/">
            <Logo>
              <img src="/assets/GCNLogo.svg" />
            </Logo>
          </Link>

          <Search>
            <SearchButton type="button" onClick={handleSearchToggle}>
              <img src="/assets/search.svg" alt="Search" title="search" />
            </SearchButton>
          </Search>
        </Header>

        <QuickNav>
          <P>
            <Link href="/">
              <a>Shop</a>
            </Link>
          </P>
          <P>
            <Link href="/">
              <a>Club</a>
            </Link>
          </P>
          <P>
            <Link href="/">
              <a>Events</a>
            </Link>
          </P>
        </QuickNav>
      </Wrapper>

      <Nav active={activeNav} />

      {activeSearch && (
        <SearchView>
          <h2>Search</h2>
          <CloseButton onClick={handleSearchToggle}>X </CloseButton>
          <SearchInput type="text" value={searchTerm} onChange={setSearchTerm} />

          <Videos inline videos={searchResults} />
        </SearchView>
      )}
    </>
  );
};
