import { Fragment } from "react";
import styled from "styled-components";
import Link from "next/link";
import navigation from "./nav.json";
import styles from "../../utils/styles";
import { dim } from "../../utils";

import SubNav from "./SubNav";

interface Props {
  active: boolean;
}

const navWidth = 22;

const Nav = styled.nav`
  height: 100vh;
  width: ${dim(navWidth)};
  position: fixed;
  top: 141px;
  left: ${({ active }: { active: boolean }) => (active ? 0 : dim(navWidth * -1))};
  transition: left ease-in-out 0.3s;
  overflow: scroll;
  border-right: 1px solid ${styles.colors.grey};
  padding-left: ${dim()};
  background-color: ${styles.colors.white};
`;

const Div = styled.div`
  position: relative;
`;

const NavList = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;

  li,
  li a {
    color: black;
    text-decoration: underline;
  }
`;

const NavListItem = styled.li`
  margin: 0;
  padding: ${dim()} 0;
  & + & {
    border-top: 1px solid ${styles.colors.grey};
  }
`;

export default ({ active }: Props) => {
  return (
    <Div>
      <Nav active={active}>
        <NavList>
          {navigation.items.map((item, i) => (
            <Fragment key={i}>
              {item.items ? (
                <SubNav item={item} />
              ) : (
                <NavListItem>
                  <Link href="/post/[id]" as={`/post/${item.title}`}>
                    <a>{item.title}</a>
                  </Link>
                </NavListItem>
              )}
            </Fragment>
          ))}
        </NavList>
      </Nav>
    </Div>
  );
};
