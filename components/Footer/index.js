import styled from "styled-components";
import data from "./footer.json";
import styles from "../../utils/styles";
import { dim } from "../../utils";

const Footer = styled.footer`
  border-top: 1px solid ${styles.colors.grey};
  padding: ${dim(2)} 0;
`;

const Social = styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const Icon = styled.img`
  width: ${dim(3)};
`;

export default () => (
  <Footer>
    <Social>
      {data.items.map((item, i) => (
        <li key={i}>
          <Icon src={`/assets/${item.icon}`} />
        </li>
      ))}
    </Social>
  </Footer>
);
