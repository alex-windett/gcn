import styled from "styled-components";
import styles from "../../utils/styles";
import { dim } from "../../utils";

const Button = styled.button`
  background-color: ${styles.colors.brand};
  color: ${styles.colors.white};
  border: none;
  appearance: none;
  border: none;
  border-radius: 0;
  padding: ${dim()} ${dim(2)};
  text-transform: uppercase;

  &:hover {
    background-color: ${styles.colors.brandHover};
  }
`;

export default ({ children, ...props }: any) => <Button {...props}>{children}</Button>;
