# PlaySports Group

## Deployment

[https://gcn.alex-windett.vercel.app/](https://gcn.alex-windett.vercel.app/)

## .env

The video url provided in the instructions needs to be added here set as `VIDEO_URL`

```
VIDEO_URL=https://www.globalcyclingnetwork.com/XX/XX
```

## Installation

`yarn` or `npm install`

### Development

`yarn dev` or `npm run dev`

### Production

`yarn build; yarn start` or `npm run build; npm run start`;

#### Notes

The categorisation dropdown currently doesnt render any new content, but I would expect this to be linked to the tags array in the results JSON.
