import React, { FC } from "react";
import { AppProps } from "next/app";
import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
body {
	margin: 0
}

img { max-width: 100%; }
`;

const CustomApp: FC<AppProps> = ({ Component, pageProps }) => (
  <>
    <GlobalStyles />
    <Component {...pageProps} />
  </>
);

export default CustomApp;
