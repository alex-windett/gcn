import Document, { Html, Head, Main, NextScript } from "next/document";
import { DocumentContext } from "next/document";
import { ServerStyleSheet } from "styled-components";

import styles from "../utils/styles";

interface IDocument {
  styleTags: string;
}

class MyDocument extends Document<IDocument> {
  static async getInitialProps(ctx: DocumentContext) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) => sheet.collectStyles(<App {...props} />),
        });

      const initialProps = await Document.getInitialProps(ctx);
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      };
    } finally {
      sheet.seal();
    }
  }

  render() {
    return (
      <Html>
        <Head>
          <title>Global Cycling Network - Home | GCN</title>
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="https://www.globalcyclingnetwork.com/imgs/favicons/apple-touch-icon.png"
          />
          <link
            rel="icon"
            type="image/png"
            href="https://www.globalcyclingnetwork.com/imgs/favicons/favicon-32x32.png"
            sizes="32x32"
          />
          <link
            rel="icon"
            type="image/png"
            href="https://www.globalcyclingnetwork.com/imgs/favicons/favicon-16x16.png"
            sizes="16x16"
          />
          <link rel="manifest" href="/manifest.json" />
          <meta name="theme-color" content={styles.colors.brand} />
          <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css.map"
          />
          <link rel="stylesheet" href="https://jonsuh.com/hamburgers/css/screen.css" />

          {this.props.styleTags}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
