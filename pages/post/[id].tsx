import Layout from "../../containers/Layout";
import { useRouter } from "next/router";

export default () => {
  const router = useRouter();

  return (
    <Layout>
      <h1>Page for {router.query.id} </h1>
    </Layout>
  );
};
