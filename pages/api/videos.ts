import Axios from "axios";
import { NextApiResponse } from "next";

export default async (_: any, res: NextApiResponse) => {
  try {
    const { data } = await Axios({
      method: "get",
      url: process.env.VIDEO_URL,
    });

    return res.send(data);
  } catch (err) {
    console.log(err);

    res.status(500).send(err);
  }
};
