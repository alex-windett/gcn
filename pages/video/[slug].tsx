import React from "react";
import { GetServerSideProps } from "next";
import Layout from "../../containers/Layout";
import Iframe from "../../components/Iframe";
import axios from "axios";

import { VideoInterface } from "../../types";

// @ts-ignore
export const getServerSideProps: GetServerSideProps = async ({ params }: {}) => {
  const { data } = await axios({
    method: "get",
    url: process.env.VIDEO_URL,
  });

  const videos: VideoInterface[] = data;

  const video: VideoInterface | undefined = videos.find(
    // @ts-ignore
    (video: VideoInterface) => video.urlTitle === params.slug
  );

  return {
    props: {
      video,
    },
  };
};

const Video: React.FC<{ video: VideoInterface }> = ({ video }) => {
  return (
    <Layout>
      <Iframe src={`https://www.youtube.com/embed/${video._id}`} />
    </Layout>
  );
};

export default Video;
