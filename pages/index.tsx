import { useState } from "react";
import { InferGetStaticPropsType } from "next";
import axios from "axios";
import styled from "styled-components";

import { VideoInterface } from "../types";

import Videos from "../components/VideosGrid";
import Select from "../components/Select";
import Layout from "../containers/Layout";
import Button from "../components/Button";
import { dim } from "../utils";

const StyledButton = styled(Button)`
  margin: ${dim(2)} auto;
  display: block;
`;

const Wrapper = styled.div`
  padding: 0 ${dim(3)};
`;

const H1 = styled.h1`
  margin-top: ${dim(1)};
  font-size: 24px;
`;

export const getStaticProps = async () => {
  const { data } = await axios({
    method: "get",
    url: process.env.VIDEO_URL,
  });

  const videos: VideoInterface[] = data;

  return {
    props: {
      videos,
    },
  };
};

const Home = ({ videos }: InferGetStaticPropsType<typeof getStaticProps>) => {
  const [items, setItems] = useState(videos.slice(0, 10));

  const handleClick = () => {
    setItems(videos);
  };

  return (
    <Layout>
      <Wrapper>
        <H1>Our Latest Videos</H1>

        <Select>
          <option disabled>Browse categories</option>
          <option>All</option>
          <option>How To</option>
          <option>Maintenance</option>
          <option>Ask GCN</option>
          <option>Training</option>
          <option>Features</option>
          <option>Top 10s</option>
          <option>GCN Racing</option>
          <option>GCN Tech</option>
        </Select>

        <Videos videos={items} />

        {items.length < videos.length && (
          <StyledButton onClick={handleClick}>Load All</StyledButton>
        )}
      </Wrapper>
    </Layout>
  );
};

export default Home;
